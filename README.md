LEPTON
========

Official repository for cms LEPTON.

Thank you for visiting and using LEPTON.
Please notice following details:

1. We use the documentation to inform about modifications of LEPTON CMS, developers as well as agencies and users. We are sure these informations are helpful.
https://doc.lepton-cms.org

2. Please don't use the repository for productive installations, because this is a developers SVN. 
https://gitlab.com/lepton-cms/LEPTON
You can download all available packages on our homepage 
https://lepton-cms.org
in the download area!

3. Please report all issues from current stable release to issue tracker in the svn:
https://gitlab.com/lepton-cms/LEPTON/issues
but please have a look on our forum
http://forum.lepton-cms.org/
and also on the closed issues
https://gitlab.com/lepton-cms/LEPTON/issues?scope=all&utf8=%E2%9C%93&state=closed
before you post a new issue.
Thanks.

For more information about LEPTON project visit our homepage.
